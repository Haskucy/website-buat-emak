from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from .views import profil
from django.http import HttpRequest
from .models import Status
from datetime import datetime
from .forms import Status_Form
from selenium import webdriver
import unittest
import time

class PostStatusTest(TestCase):

    def test_poststatus_url_ada(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_poststatus_fungsi_index(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_poststatus_isi_html(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Halo, apa kabar?', html_response)

    def test_poststatus_model_bikin_status(self):
        testStatus = Status.objects.create(
        status = "Hello World",
        waktuPost = datetime.now()
        )

        jumlahStatusYangTelahDibuat = Status.objects.all().count()
        self.assertEqual(jumlahStatusYangTelahDibuat, 1 )

    def test_poststatus_form_nggakValid_kaloKosong(self):
        form = Status_Form(data={'status' : ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['status'], ['This field is required.'])

    def test_poststatus_url_profil_ada(self):
        response = Client().get('/profil')
        self.assertEqual(response.status_code, 200)

    def test_poststatus_fungsi_profil(self):
        found = resolve('/profil')
        self.assertEqual(found.func, profil)

    def test_poststatus_isi_html_profil(self):
        request = HttpRequest()
        response = profil(request)
        html_responseNama = response.content.decode()
        self.assertIn('Nama', html_responseNama)
        html_responseFoto = response.content.decode()
        self.assertIn('<img', html_responseFoto)
        html_responseNPM = response.content.decode()
        self.assertIn('NPM', html_responseNPM)


class VisitorUsesPostStatus(unittest.TestCase):
    def setUp(self):
        self.browser = webdriver.Firefox()

    def tearDown(self):
        self.browser.quit()

    def test_isi_poststatus_dan_muncul_di_page(self):
        self.browser.get('http://127.0.0.1:8000/')

        time.sleep(5)

        search_box = self.browser.find_element_by_id("id_status")
        search_box.send_keys("Coba-coba")
        search_box.submit()

        time.sleep(2)

        self.assertIn("Coba-coba", self.browser.page_source)

if __name__ == '__main__':
    unittest.main(warnings = 'ignore')
