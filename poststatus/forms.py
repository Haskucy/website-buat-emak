from django import forms
from .models import Status

class Status_Form(forms.ModelForm):
    class Meta:
        model = Status
        fields = ['status']
