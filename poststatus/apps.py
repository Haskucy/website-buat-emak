from django.apps import AppConfig


class PoststatusConfig(AppConfig):
    name = 'poststatus'
