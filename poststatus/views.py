from django.shortcuts import render
from .forms import Status_Form
from .models import Status
import datetime

def index(request):
    if request.method == "POST":
        form = Status_Form(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.published_date = datetime
            post.save()
        isi = Status.objects.all()
        return render(request, 'index.html', {'form' : form, 'isi' : isi})
    else:
        form = Status_Form()
        isi = " "
    return render(request, 'index.html', {'form' : form} )

def profil(request):
    return render(request, 'profil.html')

# Create your views here.
